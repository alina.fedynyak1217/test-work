let chess = [];
for (let i = 0; i < 8; i++) {
    chess[i] = [];
    for (let j = 0; j < 8; j++) {
        if (i === j) {
            chess[i][j] = 1;
            continue;
        }
        chess[i][j] = 0;
    }
}
console.log(chess);